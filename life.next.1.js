"use strict";

const SAVE = [2, 3]; // array of numbers for save point in next generation
const NEWLIFE = [3]; // array of numbers for new point in next generation

// log generation
function print(gen) {
  let arr = Array.from(gen.keys());
  console.log(arr.join(" - "));
}

// list of POINT -> Generation
class Life {
  constructor() {
    this.gen = new Map(); // coord of points in current generation
    this.env = new Map(); // environment points (coord)
    this.next = new Map(); // next generation
    this.genNum = 0;
    this.renderFunction = undefined;
  }
  // add from x, y
  add(x, y) {
    let coord = Array.from(arguments);
    this.gen.set(this.key(coord), coord);
    return this;
  }

  setRenderFunction(f) {
    this.renderFunction = f;
  }

  key(arr) {
    return arr.join(".");
  }

  renderGen() {
    //console.log("gen=0, " + this.gen.size);
    //print(this.gen);
    this.renderFunction(this.gen);
  }

  renderNext(time = 0) {
    // console.log(
    //   "gen=" + this.genNum + ", " + this.next.size + " points, " + time + " sec"
    // );
    this.renderFunction(this.next);
    //print(this.next);
  }

  run(width, height) {
    this.width = width;
    this.height = height;
    this.genNum = 0;
    this.renderGen();
    return { genNum: this.genNum, pCount: this.gen.size };
  }

  cicle() {
    this.genNum++;
    let start = new Date();
    this.getNext();
    let time = (new Date() - start) / 1000;
    this.renderNext(time);
    let pCount = this.next.size;
    this.gen = this.next;
    this.next = new Map();
    this.env = new Map();
    return { genNum: this.genNum, pCount: pCount };
  }

  // check coord in [0, 0, width, height]
  check(coord) {
    if (coord[0] < 0) {
      coord[0] += this.width;
      coord[1] = this.height - coord[1]; // inversion
    }
    if (coord[0] >= this.width) {
      coord[0] -= this.width;
      coord[1] = this.height - coord[1];
    }
    if (coord[1] < 0) {
      coord[1] += this.height;
      coord[0] = this.width - coord[0];
    }
    if (coord[1] >= this.height) {
      coord[1] -= this.height;
      coord[0] = this.width - coord[0];
    }
    return coord;
  }

  //cicle by gen of points
  getNext() {
    //
    this.gen.forEach((coord) => {
      let envVal = this.getEnvValue(coord);
      // if env variable in SAVE array
      if (SAVE.includes(envVal)) {
        let checkCoord = this.check(coord);
        this.next.set(this.key(checkCoord), checkCoord);
      }
    });
    //
    this.env.forEach((coord) => {
      let envVal = this.getEnvValue(coord, false);
      // if env variable in NEWLIFE array
      if (NEWLIFE.includes(envVal)) {
        let checkCoord = this.check(coord);
        this.next.set(this.key(checkCoord), checkCoord);
      }
    });
  }

  // Environment Variable =  count of points from Table(3x3)
  // and add point to env if calcEnv
  getEnvValue(coord, calcEnv = true) {
    let val = 0;
    let table = this.getTable(coord);
    table.forEach((tableCoord) => {
      let checkTableCoord = this.check(tableCoord);
      if (this.gen.has(this.key(checkTableCoord))) {
        // there is a point here
        val++;
      } else {
        // this is point from env
        if (calcEnv) {
          // if this is new env point
          if (!this.env.has(this.key(checkTableCoord)))
            this.env.set(this.key(checkTableCoord), checkTableCoord);
        }
      }
    });
    return val;
  }
  // return 3x3 table
  getTable(coord) {
    let envTable = [
      [0, 1],
      [1, 1],
      [1, 0],
      [1, -1],
      [0, -1],
      [-1, -1],
      [-1, 0],
      [-1, 1],
    ];
    envTable.forEach((elem) => {
      elem[0] += coord[0];
      elem[1] += coord[1];
    });
    return envTable; // coord of points [x, y], [x, y], ...
  }
}

let life;
let canvas;
let timer;
let interval;
let normalRandom;
let genNum;
let pCount;
let scale;
let dx = 0;
let dy = 0;
let olddx;
let olddy;

let mDown = false;
let downPoint = { x: 0, y: 0 };

function runLife() {
  let PointCount = Number(document.getElementById("pointCount").value);
  let mapSizeW = Number(document.getElementById("mapSizeW").value);
  let mapSizeH = Number(document.getElementById("mapSizeH").value);
  let size = Number(document.getElementById("size").value);
  interval = Number(document.getElementById("interval").value);
  normalRandom = document.getElementById("normR").checked;
  // out
  genNum = document.getElementById("genNum");
  pCount = document.getElementById("pCount");
  // canavs & scale
  canvas = document.getElementById("life");
  // canvas.style.width = mapSizeW + "px";
  // canvas.style.height = mapSizeH + "px";
  canvas.width = mapSizeW;
  canvas.height = mapSizeH;
  scale = Number(document.getElementById("scale").value);
  olddx = dx = (canvas.width * scale - canvas.width) / 2;
  olddy = dy = (canvas.height * scale - canvas.height) / 2;

  life = new Life();
  life.setRenderFunction(render);
  for (let i = 0; i < PointCount; i++) {
    let coord = randomPointinEllipce(mapSizeW, mapSizeH, size / 100);
    life.add(coord.x, coord.y);
  }
  let out = life.run(mapSizeW, mapSizeH);
  genNum.value = out.genNum;
  pCount.value = out.pCount;
}

function randomPointinEllipce(w, h, size = 0.8) {
  let phi = Math.random() * Math.PI * 2;
  let r = normalRandom ? Math.sqrt(Math.random()) : Math.random(); // //
  let x = Math.floor(w / 2 + (r * Math.cos(phi) * w * size) / 2);
  let y = Math.floor(h / 2 + (r * Math.sin(phi) * h * size) / 2);
  return { x: x, y: y };
}

function changeScale() {
  let realX = (dx + canvas.width / 2) / scale;
  let realY = (dy + canvas.height / 2) / scale;

  scale = Number(document.getElementById("scale").value);
  dx = realX * scale - canvas.width / 2;
  dy = realY * scale - canvas.height / 2;

  console.log("Scale: " + scale + "D: " + dx + ", " + dy);
}

function center() {
  dx = ((scale - 1) * canvas.width) / 2;
  dy = ((scale - 1) * canvas.height) / 2;
}

function pointToCenter(x, y) {
  let realP = realCoord(x,y)
  dx = realP.x * scale - canvas.width / 2;
  dy = realP.y * scale - canvas.height / 2;
}

function realCoord(mouseX, mouseY) {
  let realX = (mouseX + dx) / scale;
  let realY = (mouseY + dy) / scale;
  return { x: realX, y: realY };
}

function realPointToPoint(realP, x, y) {
  dx = realP.x*scale-x
  dy = realP.y*scale-y
}

function takeC(event) {
  downPoint.x = event.offsetX;
  downPoint.y = event.offsetY;
  mDown = true;
  olddx = dx;
  olddy = dy;
  let realX = (downPoint.x + dx) / scale;
  let realY = (downPoint.y + dy) / scale;
  console.log(
    "DOWN: " +
      downPoint.x +
      ", " +
      downPoint.y +
      " Real: " +
      realX +
      ", " +
      realY
  );
  if (event.shiftKey || event.ctrlKey) {
    // let realX = (downPoint.x + dx) / scale;
    // let realY = (downPoint.y + dy) / scale;
    // dx = realX * scale - canvas.width / 2;
    // dy = realY * scale - canvas.height / 2;
    //pointToCenter(downPoint.x, downPoint.y);
    center()
  }
}

function moveC(event) {
  if (mDown) {
    let mdx = event.offsetX - downPoint.x;
    let mdy = event.offsetY - downPoint.y;

    dx = olddx - mdx;
    dy = olddy - mdy;

    console.log("MOVE: " + mdx + ", " + mdy + " D: " + dx + ", " + dy);
  }
}

function freeC(event) {
  console.log("UP: ");
  mDown = false;
}

function render(map) {
  let ctx = canvas.getContext("2d");
  ctx.fillStyle = "lightyellow";
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  map.forEach((coord) => {
    ctx.fillRect(coord[0] * scale - dx, coord[1] * scale - dy, scale, scale);
  });
}

function cicle() {
  let out = life.cicle();
  genNum.value = out.genNum;
  pCount.value = out.pCount;
}

function start() {
  timer = setInterval(cicle, interval);
}

function stop() {
  clearInterval(timer);
}

function START() {
  document.getElementById("mapSizeW").value =
    document.getElementById("life").clientWidth;
  document.getElementById("mapSizeH").value =
    document.getElementById("life").clientHeight;
  runLife();
  start();
}

START();
