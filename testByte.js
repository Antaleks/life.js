m1 = process.memoryUsage().heapUsed;

const ITER = 100000000;
const VAL = 789123.789;
// целая часть
function mInt(c) {
  for (i = 0; i < c; i++) {
    r = VAL - (VAL % 1);
  }
}
// целая часть встоенной функцией
function mIntTrunc(c) {
  for (i = 0; i < c; i++) {
    r = Math.trunc(VAL);
  }
}

// время на вуполнение функции
function testTime(func, iter = ITER) {
  st = new Date();
  func(iter);
  fin = new Date();
  res = fin - st;
  console.log(res);
}

// адрес i-го бита в цепочке битов => индекс байта, индекс бита
function bitAddres(i) {
  return { B: Math.trunc(i / 8), b: i % 8 };
}

// индекс бита по координатам х,у в массиве w*h  в цепочке битов => индекс байта, индекс бита
function xyBitIndex(x, y, w, h) {
  if (y < h && x < w) {
    return y * w + x;
  } else {
    return undefined;
  }
}
// количество байтов для массива битов w*h
function byteArraySize(w, h) {
  return Math.ceil(w * h);
}
// 
const bitArr = [5, 7, 8, 9, 10, 11, 12, 16, 18, 19, 23, 24, 25, 99];
function testBitAddres(arr) {
  arr.forEach((e) => {
    res = bitAddres(e);
    console.log(`${e} = ${res.B}, ${res.b}`);
  });
}

// максимальній массив 1000*1000*10(20, ... 50) - норм
function maxArray() {
  size = 1e7;
  arr = [];
  for (i = 0; i < size; i++) {
    arr.push(i*i);
  }
}

// testTime(mInt);
// testTime(mIntTrunc);
// testBitAddres(bitArr);
testTime(maxArray);

m2 = process.memoryUsage().heapUsed;

console.log(
  `usage memory: ${m1 / 1024 / 1024}, ${m2 / 1024 / 1024}, ${
    (m2 - m1) / 1024 / 1024
  }`
);

